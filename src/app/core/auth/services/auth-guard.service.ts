import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanActivateChild, CanLoad, Route
} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
	providedIn: 'any'
})
export class AuthGuard implements CanActivate {

    constructor(protected authService: AuthService, protected router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const url: string = state.url;

        return this.checkLogin(url, route);
    }

    checkLogin(url: string, route: ActivatedRouteSnapshot): boolean {
        if (this.authService.isLoggedIn) {
            // route.data = {...route.data, currentlyLoggedUser: {i: 1} };
            
            return true;
        }

        // Navigate to the login page
        this.router.navigate(['/']);
        // the above navigation automatically cancels the current navigation, return false just to be clear
        return false;
    }
}
