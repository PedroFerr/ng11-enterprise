import { Component, OnInit, Input } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';

import { AuthService } from '../../auth/services/auth.service';
import { ThemeColorService } from 'src/app/services/theme-color.service';

@Component({
	selector: 'app-sidenav',
	templateUrl: './sidenav.component.html',
	styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {

	themeColor: 'primary' | 'accent' | 'warn';
	isDark = false;

	constructor(
		public authService: AuthService,
		private overlayContainer: OverlayContainer,
		private themeColorService: ThemeColorService,
	) {	
		if(localStorage.getItem('c')) {
			this.themeColor = localStorage.getItem('c') as 'primary' | 'accent' | 'warn';
		} else {
			this.themeColor = 'primary';
			localStorage.setItem('c', 'primary');
		}

	}

	ngOnInit() {
		this.switchTheme(this.themeColor);
	}

	toggleTheme() {

		this.isDark = !this.isDark;

		if (this.isDark) {
			this.overlayContainer.getContainerElement().classList.add('dark-theme');
		} else {
			this.overlayContainer.getContainerElement().classList.remove('dark-theme');
		}
	}

	switchTheme(color: 'primary' | 'accent' | 'warn') {
		console.log(color);

		// MUST arrived SOME different color...
		localStorage.removeItem('c');
		localStorage.setItem('c', color);

		this.themeColorService.passedVar(color);
	}
}