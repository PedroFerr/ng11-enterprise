/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LinkL1Component } from './link-l1.component';

describe('LinkL1Component', () => {
  let component: LinkL1Component;
  let fixture: ComponentFixture<LinkL1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkL1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkL1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
