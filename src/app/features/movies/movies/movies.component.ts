import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { MoviesService } from '../services/movies.service';

import { ThemeColorService } from 'src/app/services/theme-color.service';
import { ThemeColorLib } from 'src/app/z-lib/theme-color.lib';

interface Movie {
    id: number;
    name: string;
    email: string;
}

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls: ['./movies.component.scss']
})

export class MoviesComponent extends ThemeColorLib implements OnInit, AfterViewInit, AfterViewChecked {

    myControl = new FormControl();
    name = ''; // For the 'search' (field)...

    constructor(
        private cdRef: ChangeDetectorRef,
        private moviesService: MoviesService,
        public themeColorService: ThemeColorService
    ) {
        super(themeColorService);
    }

    // options: string[] = [
    //     'Rajkot',
    //     'Surat',
    //     'Vapi',
    //     'Bhavnagar',
    //     'Delhi',
    //     'Mumbai',
    //     'Banglore'
    // ];
    options: Array<any> = [];

    options$: Observable<Movie[]> = this.moviesService.getMovies();

    filteredOptions$: Observable<string[]> | undefined;

    @ViewChild('lastElement') lastElement: ElementRef | undefined;
    @ViewChild('separator') separator: ElementRef | undefined;
    @ViewChild('container') container: ElementRef | undefined;

    divSeparator: HTMLDivElement | undefined;
    divContainer: HTMLDivElement | undefined; // Area with all the 'cards', below search bar
    
    ngOnInit() {
        this.initMakeSearchArray();
        this.initSearch();
    }

    ngAfterViewInit() {
        // const divEl: HTMLDivElement = this.lastElement?.nativeElement.getAttribute('data-counter');
        // console.log('Nº elems : ', divEl);
        this.divSeparator = this.separator?.nativeElement;
        this.divContainer = this.container?.nativeElement.querySelectorAll('.row')[0];
    }

    ngAfterViewChecked() {
        //explicit change detection to avoid "expression-has-changed-after-it-was-checked-error"
        this.cdRef.detectChanges();

        // Tem de ser neste! Cada input novo faz o 'change' => novo nº no inputs no input => o Angular checka todo o front end...
        this.getHeightBelow()
    }

    clearSearchField() {
        this.name = '';
        // THIS causes error (ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked) WITHOUT
        // private cdRef: ChangeDetectorRef AND detect changes 'ngAfterViewChecked'
    }

    private initMakeSearchArray() {
        this.options$.subscribe((options: Movie[]) => {
            // --------------------------------------------------------------------------------------
            // options.forEach(e => this.options.push(e));
            // options.forEach(e => this.options.push(e.id + ' - ' + e.name + ' (' + e.email + ')'));
            options.forEach(e => this.options.push(e.id + ' ' + e.name)); // Need to have 'e.id' => so props get done! For instances:
            // last as lastItem;
            // <img mat-card-avatar src="https://reqres.in/img/faces/{{i+1}}-image.jpg" alt="Your photo!" />
            // index as i;
            // --------------------------------------------------------------------------------------
            console.log(this.options)
        })
    }

    private getHeightBelow() {
        const divOption: HTMLDivElement = this.lastElement?.nativeElement.getAttribute('data-counter');
        if(divOption !== undefined) {
            const hOption = 48;
            const maxHeight = 256;
            const totalHeight = Number(divOption) * hOption;

            const realHeight = totalHeight >= maxHeight ? maxHeight : totalHeight;
            if(this.divSeparator) {
                this.divSeparator.style.height = realHeight.toString() + 'px';
            }
        } else {
            if(this.divSeparator) {
                this.divSeparator.style.height = '0px';
            }
        }
    }

    private initSearch() {
        this.filteredOptions$ = this.myControl.valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();

        if (filterValue === '') {
            this.allCardsFullOpacity();
            return [];
        }

        const filterElem = this.options.filter(option => option.toLowerCase().includes(filterValue));
        
        // Just 'delete' cards on the side; we keep coming and back to here...
        this.deleteCardsBelow(filterElem);

        return filterElem;
    }

    private deleteCardsBelow(cards: string[]) {
        const elemsLive: number[] = cards.map((x: string) => Number(x.substr(0, 2).trim()));
        return this.cardsOpacity(elemsLive);
    }

    private allCardsFullOpacity() {
        this.divContainer?.childNodes.forEach(child => {
            const card = child.childNodes[0] as HTMLDivElement;
            if(card) {  
                card.style.opacity = '1';
            }
        })
    }

    private cardsOpacity(elemsLive: number[]) {
        const deleteCards: Array<any> = [];

        this.divContainer?.childNodes.forEach(
            (child, idx) => {
                const card = child.childNodes[0] as HTMLDivElement;

                if(!elemsLive.includes(idx + 1)) {
                    deleteCards.push(card)
                } else {
                    card.style.opacity = '1';
                }
            }
        );
        // console.log(deleteCards);

        for (let index = 0; index < deleteCards.length; index++) {
            const del: HTMLDivElement = deleteCards[index];
            if(del) {    
                del.style.opacity = '0.2';
            }      
        }

    }

    trackByFn(index: number, option: string) {
        return index; // or 'options.id', unique id corresponding to the item <= ONLY 'options' is Array!!!
    }

}
