import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { FeatureChartsRoutingModule } from './feature-charts-routing.module';

import { ChartsComponent } from './charts/charts.component';
import { ScatterLineChartComponent } from './charts/scatter-line-chart/scatter-line-chart.component';

@NgModule({
	imports: [
		CommonModule,		
        FeatureChartsRoutingModule,

		NgxChartsModule
	],
	declarations: [
		ChartsComponent,
		ScatterLineChartComponent
    ],
	providers: [],
})
export class FeatureChartsModule { }
