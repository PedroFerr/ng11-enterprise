import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';

import { AuthService } from '../services/auth.service';
import { ThemeColorService } from 'src/app/services/theme-color.service';
import { ThemeColorLib } from 'src/app/z-lib/theme-color.lib';
import { NotificationService } from 'src/app/services/notification.service';

import { User } from '../auth.interfaces';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent extends ThemeColorLib implements OnInit {

	imageLoader = true;

	// loginForm: FormGroup | undefined;
	loginForm: FormGroup = new FormGroup({
		email: new FormControl(),
		password: new FormControl(),
	});
	
	isLoggedIn = false;
	user: User | undefined;
	
	email: string | undefined;
	authToken: string | undefined;

	constructor(
		private fb: FormBuilder,
		private authService: AuthService,
		public themeColorService: ThemeColorService,
		private notification: NotificationService
	) { super(themeColorService); }

	ngOnInit() {
		this.loginForm = this.fb.group({
			email: [
				'',
				[Validators.required, Validators.pattern('[a-z0-9.@]*')]
			],
			password: [
				'',
				[Validators.required, Validators.minLength(4)]
			]
		})

		this.seeIfUserIsLoggedIn();
		
		this.isLoggedIn = this.authService.isLoggedIn;
	}

	onSubmit(): void {
		if(this.loginForm?.valid) {
			this.authService.login(
				this.loginForm?.get('email')?.value,
				this.loginForm?.get('password')?.value
			)
			.subscribe((resp: User) => {
				// console.log(resp);
				localStorage.setItem('authData', JSON.stringify({
					auth_token: resp.token,
					email: resp.email
				}));

				this.seeIfUserIsLoggedIn();
			});
		}
	}

	logout(): void {
		this.authService.logout();
		this.isLoggedIn = false;
		this.notification.info('You are now logged out.');
	}

	seeIfUserIsLoggedIn() {
		const authData: any = localStorage.getItem('authData');
		if(authData) {
			this.email = JSON.parse(authData).email;
			this.authToken = JSON.parse(authData).auth_token;

			this.isLoggedIn = true;
		}
	}
}
