import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root',
})
export class NotificationService {
    
    constructor(private readonly snackBar: MatSnackBar) { }

    default(message: string): void {
        this.show(message, {
            duration: 3000,
            panelClass: 'default-notification-overlay',
        });
    }

    info(message: string): void {
        this.show(message, {
            duration: 3000,
            panelClass: 'info-notification-overlay',
        });
    }

    success(message: string): void {
        this.show(message, {
            duration: 3000,
            panelClass: 'success-notification-overlay',
        });
    }

    warn(message: string): void {
        this.show(message, {
            duration: 4000,
            panelClass: 'warning-notification-overlay',
        });
    }

    error(message: string): void {
        this.show(message, {
            duration: undefined,
            panelClass: 'error-notification-overlay',
        }, 'OK');
    }

    private show(message: string, configuration: MatSnackBarConfig, action?: string): void {

        configuration.horizontalPosition = 'right';
        configuration.verticalPosition = 'bottom';

        this.snackBar.open(message, action, configuration);
    }
}