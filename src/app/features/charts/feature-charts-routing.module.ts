import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'src/app/core/auth/services/auth-guard.service';

import { ChartsComponent } from './charts/charts.component';

const routes: Routes = [
    { path: '', component: ChartsComponent, canActivate: [AuthGuard]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FeatureChartsRoutingModule { }
