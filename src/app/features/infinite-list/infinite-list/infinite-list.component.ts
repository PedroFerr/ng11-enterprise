import { Component, OnInit, ChangeDetectionStrategy, ViewChild, AfterViewInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { map, pairwise, filter, throttleTime } from 'rxjs/operators';
import { timer } from 'rxjs';

import { ThemeColorService } from 'src/app/services/theme-color.service';
import { ThemeColorLib } from 'src/app/z-lib/theme-color.lib';

@Component({
    selector: 'app-infinite-list',
    templateUrl: './infinite-list.component.html',
    styleUrls: ['./infinite-list.component.scss'],
})
export class InfiniteListComponent extends ThemeColorLib implements OnInit, AfterViewInit {

    @ViewChild('scroller') scroller: CdkVirtualScrollViewport | undefined;

    listItems: Array<any> = [];
    loading = false;

    constructor(
        private ngZone: NgZone,
        public themeColorService: ThemeColorService
    ) { super(themeColorService); }

    ngOnInit(): void {
        this.fetchMore();
    }

    ngAfterViewInit(): void {

        this.scroller?.elementScrolled().pipe(
            map(() => this.scroller?.measureScrollOffset('bottom')),
            pairwise(),
            filter(([y1, y2]) => ( y1 !== undefined && y2 !== undefined && y2 < y1 && y2 < 140)),
            throttleTime(200)
        ).subscribe(() => {
            this.ngZone.run(() => {
                this.fetchMore();
            });
        });
    }

    fetchMore(): void {

        const images = ['IuLgi9PWETU', 'fIq0tET6llw', 'xcBWeU4ybqs', 'YW3F-C5e8SE', 'H90Af2TFqng'];
        const newItems: Array<any> = [];
    
        for (let i = 0; i < 12; i++) {
            const randomListNumber = Math.round(Math.random() * 100);
            const randomPhotoId = Math.round(Math.random() * 4);
            newItems.push({
                title: 'List Item ' + randomListNumber,
                content: 'This is some description of the list - item # ' + randomListNumber,
                image: `https://source.unsplash.com/${images[randomPhotoId]}/50x50`
            });
        }

        this.loading = true;
        timer(1000).subscribe(() => {
            this.loading = false;
            this.listItems = [...this.listItems, ...newItems];
        });

    }

}