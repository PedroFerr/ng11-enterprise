import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureInfiniteListRoutingModule } from './feature-infinite-list-routing.module';

import { CoreMaterialModule } from 'src/app/core/material/material.module';

import { InfiniteListComponent } from './infinite-list/infinite-list.component';

@NgModule({
	imports: [
		CommonModule,
		CoreMaterialModule,

		FeatureInfiniteListRoutingModule,
	],
	declarations: [
		InfiniteListComponent,
    ],
	providers: [],
})
export class FeatureInfiniteListModule { }
