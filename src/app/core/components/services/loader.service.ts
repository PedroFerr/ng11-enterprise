import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { defer, NEVER } from 'rxjs';
import { finalize, share } from 'rxjs/operators';
import { AppLoaderComponent } from '../loader/loader.component';

@Injectable({
    providedIn: 'root',
})
export class LoaderService {

    private overlayRef: OverlayRef | undefined = undefined;

    constructor(private readonly overlay: Overlay) { }

    public readonly spinner$ = defer(() => {
        this.showLoader();
        return NEVER.pipe(
            finalize(() => {
                this.hideLoader();
            })
        );
    }).pipe(share());

    showLoader(): void {
        console.log('LoaderService ~ show spinner');
        
        // Hack avoiding `ExpressionChangedAfterItHasBeenCheckedError` error
        Promise.resolve(null).then(() => {
            this.overlayRef = this.overlay.create({
                positionStrategy: this.overlay
                    .position()
                    .global()
                    .centerHorizontally()
                    .centerVertically(),
                hasBackdrop: true,
            });
            this.overlayRef.attach(new ComponentPortal(AppLoaderComponent));
        });
    }

    hideLoader(): void {
        console.log('LoaderService ~ hide spinner');
        
        this.overlayRef?.detach();
        this.overlayRef = undefined;
    }
}