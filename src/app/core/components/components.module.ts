import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreMaterialModule } from '../material/material.module';
import { ComponentsRoutingModule } from './components-routing.module';

import { AppLoaderComponent } from './loader/loader.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { DialogComponent } from './dialog/dialog.component';
import { LinkL1Component } from './sidenav/link-l1/link-l1.component';
import { LinkL2Component } from './sidenav/link-l2/link-l2.component';
import { LinkL3Component } from './sidenav/link-l3/link-l3.component';
// import { AuthModule } from '../auth/auth.module';

@NgModule({
    imports: [
        CommonModule,
        // AuthModule,
        CoreMaterialModule,

        ComponentsRoutingModule,
    ],
    declarations: [
        AppLoaderComponent,
        SidenavComponent,
        DialogComponent,
        LinkL1Component,
        LinkL2Component,
        LinkL3Component
    ],
    exports: [
        AppLoaderComponent,
        SidenavComponent,
        DialogComponent,
        LinkL1Component,
        LinkL2Component,
        LinkL3Component,
    ],
    providers: []
})
export class CoreComponentsModule { }
