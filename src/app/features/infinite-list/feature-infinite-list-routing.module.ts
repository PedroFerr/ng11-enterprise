import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'src/app/core/auth/services/auth-guard.service';

import { InfiniteListComponent } from './infinite-list/infinite-list.component';

const routes: Routes = [
    { path: '', component: InfiniteListComponent, canActivate: [AuthGuard]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FeatureInfiniteListRoutingModule { }
