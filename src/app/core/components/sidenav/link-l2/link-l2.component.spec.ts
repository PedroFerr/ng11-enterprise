/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LinkL2Component } from './link-l2.component';

describe('LinkL2Component', () => {
  let component: LinkL2Component;
  let fixture: ComponentFixture<LinkL2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkL2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkL2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
