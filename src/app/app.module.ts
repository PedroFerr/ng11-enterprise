import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CoreComponentsModule } from './core/components/components.module';
import { CoreMaterialModule } from './core/material/material.module';

import { HttpRequestInterceptor } from './core/http/http-request.interceptor';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        
        CoreComponentsModule,   // 'app.component' has 'app-sidenav', from 'sidenav.component'
        CoreMaterialModule,     // ... and Material is used in 'app.component' ([color]="sidenav.themeColor")
        // AuthModule,             // We don´t 'import" it - it´s a Lazy Module!!!

        AppRoutingModule,
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
