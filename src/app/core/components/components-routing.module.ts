import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { LinkL1Component } from './sidenav/link-l1/link-l1.component';
import { LinkL2Component } from './sidenav/link-l2/link-l2.component';
import { LinkL3Component } from './sidenav/link-l3/link-l3.component';

const routes: Routes = [
    { path: 'link1', component: LinkL1Component },
    { path: 'link2', component: LinkL2Component },
    { path: 'link3', component: LinkL3Component },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ComponentsRoutingModule { }
