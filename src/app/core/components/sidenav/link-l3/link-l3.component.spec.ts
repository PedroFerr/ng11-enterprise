/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LinkL3Component } from './link-l3.component';

describe('LinkL3Component', () => {
  let component: LinkL3Component;
  let fixture: ComponentFixture<LinkL3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkL3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkL3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
