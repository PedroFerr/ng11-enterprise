import { OnInit, Component, HostListener } from '@angular/core';

import { DataProviderService } from '../services/data-provider.service';

import { ChartConfig } from './scatter-line-chart/scatter-line-series.model';
import { GraphConfig, ShotData } from '../services/shot-data';

@Component({
    selector: 'app-charts',
    templateUrl: './charts.component.html',
    styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {
    
    // GRAPH: ---------------------------------------------------
    view: any; // = [1100, 400];
    colorScheme: { domain: Array<string> } = { domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'] };

    legend: boolean = true;
    legendPosition: any = 'below'; // 'below', 'side'
    showLegend: boolean = true;

    showXAxisLabel: boolean = true;
    showYAxisLabel: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showLabels: boolean = true;

    isDoughnut: boolean = false;

    animations: boolean = true;
    timeline: boolean = true;

    // GRAPH: dig-graph-wrapper --------------------------------
    data: any;
    chartConfig: ChartConfig<ShotData>;

    // ---------------------------------------------------------
    saleData: Array<{ name: string; value: number; }> = [
        { name: "Mobiles", value: 105000 },
        { name: "Laptop", value: 55000 },
        { name: "AC", value: 15000 },
        { name: "Headset", value: 150000 },
        { name: "Fridge", value: 20000 }
    ];

    single: any = [
        {
            "name": "Germany",
            "value": 8940000
        },
        {
            "name": "USA",
            "value": 15000000
        },
        {
            "name": "France",
            "value": 7200000
        },
        {
            "name": "Italy",
            "value": 2900000
        },
        {
            "name": "UK",
            "value": 14730000
        }, {
            "name": "Portugal",
            "value": 10200000
        }
    ];

    multi: any = [
        {
            "name": "Germany",
            "series": [
                {
                    "name": "2009",
                    "value": 62000000
                },
                {
                    "name": "2010",
                    "value": 101000000
                },
                {
                    "name": "2011",
                    "value": 89400000
                }
            ]
        },

        {
            "name": "USA",
            "series": [
                {
                    "name": "2009",
                    "value": 65000000
                },
                {
                    "name": "2010",
                    "value": 70900000
                },
                {
                    "name": "2011",
                    "value": 91100000
                }
            ]
        },
        {
            "name": "France",
            "series": [
                {
                    "name": "2009",
                    "value": 58000000
                },
                {
                    "name": "2010",
                    "value": 50000020
                },
                {
                    "name": "2011",
                    "value": 58000000
                }
            ]
        },
        {
            "name": "UK",
            "series": [
                {
                    "name": "2009",
                    "value": 27000000
                },
                {
                    "name": "2010",
                    "value": 62000000
                }
            ]
        },
        {
            "name": "Portugal",
            "series": [
                {
                    "name": "2009",
                    "value": 97000000
                },
                {
                    "name": "2010",
                    "value": 22000000
                }
            ]
        }
    ]

    bubbleData: any = [
        {
            name: 'Germany',
            series: [
                {
                    name: '2010',
                    x: '2010',
                    y: 80.3,
                    r: 80.4
                },
                {
                    name: '2000',
                    x: '2000',
                    y: 80.3,
                    r: 78
                },
                {
                    name: '1990',
                    x: '1990',
                    y: 75.4,
                    r: 79
                }
            ]
        },
        {
            name: 'United States',
            series: [
                {
                    name: '2010',
                    x: '2010',
                    y: 78.8,
                    r: 310
                },
                {
                    name: '2000',
                    x: '2000',
                    y: 76.9,
                    r: 283
                },
                {
                    name: '1990',
                    x: '1990',
                    y: 75.4,
                    r: 253
                }
            ]
        },
        {
            name: 'France',
            series: [
                {
                    name: '2010',
                    x: '2010',
                    y: 81.4,
                    r: 63
                },
                {
                    name: '2000',
                    x: '2000',
                    y: 79.1,
                    r: 59.4
                },
                {
                    name: '1990',
                    x: '1990',
                    y: 77.2,
                    r: 56.9
                }
            ]
        },
        {
            name: 'United Kingdom',
            series: [
                {
                    name: '2010',
                    x: '2010',
                    y: 80.2,
                    r: 62.7
                },
                {
                    name: '2000',
                    x: '2000',
                    y: 77.8,
                    r: 58.9
                },
                {
                    name: '1990',
                    x: '1990',
                    y: 75.7,
                    r: 57.1
                }
            ]
        }
    ];
    // ---------------------------------------------------------

    @HostListener('window:resize', ['$event'])
    getViewSize(event?: any) {
        const viewWidth = document.querySelector('.container')?.clientWidth;
        this.view = [viewWidth, 400]
        // console.log('this.view = ', this.view)
    }

    constructor(
        private dataProviderService: DataProviderService
    ) {
        this.data = dataProviderService.getChartData();
        this.chartConfig = new GraphConfig();

        this.getViewSize();
    }

    ngOnInit() {
    }

    // GRAPH: advanced-pie
    onSelect(data: any): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }
    onActivate(data: any): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }
    onDeactivate(data: any): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }

}
