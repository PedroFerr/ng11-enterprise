import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScatterLineChartComponent } from '../scatter-line-chart/scatter-line-chart.component';

describe('ScatterLineChartComponent', () => {
  let component: ScatterLineChartComponent;
  let fixture: ComponentFixture<ScatterLineChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScatterLineChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScatterLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
