import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'src/app/core/auth/services/auth-guard.service';

import { MoviesComponent } from './movies/movies.component';

const routes: Routes = [
    { path: '', component: MoviesComponent, canActivate: [AuthGuard]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FeatureMoviesRoutingModule { }
