import { Directive, Injectable, OnDestroy } from "@angular/core";
import { Observable, Subscription } from "rxjs";

import { ThemeColorService } from "../services/theme-color.service";

@Injectable()
export class ThemeColorLib implements OnDestroy {

    themeColor: 'primary' | 'accent' | 'warn' = 'primary';
    subscribeToThemeColor: Subscription;

    constructor(public themeColorService: ThemeColorService) {

        const subscriber: Observable<'primary' | 'accent' | 'warn'> | undefined = this.themeColorService.themeColor$;

        this.subscribeToThemeColor = subscriber?.subscribe((color: 'primary' | 'accent' | 'warn') => {
            this.themeColor = color;
        });

    }

    ngOnDestroy() {
        if(this.subscribeToThemeColor) {
            this.subscribeToThemeColor.unsubscribe();
        }
    }
}