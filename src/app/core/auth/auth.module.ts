import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CoreMaterialModule } from '../material/material.module';
import { AuthRoutingModule } from './auth-routing.module';

import { LoginComponent } from '../auth/login/login.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule, ReactiveFormsModule,

		CoreMaterialModule,
		
		AuthRoutingModule
	],
	declarations: [
		LoginComponent
	],
	exports: [
		// LoginComponent,
	],
	providers: [],
})
export class AuthModule { }
