import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './core/auth/login/login.component';

const routes: Routes = [
    // { path: '', redirectTo: 'login', pathMatch: 'full' },
    // { path: 'login', component: LoginComponent},

    { path: '', redirectTo: '/', pathMatch: 'full' },

    // { path: 'login', loadChildren: './auth/auth.module#AuthModule' },
    { path: 'login', loadChildren: () => import('./core/auth/auth.module').then(m => m.AuthModule) },
    { path: 'charts', loadChildren: () => import('./features/charts/feature-charts.module').then(m => m.FeatureChartsModule) },
    { path: 'list', loadChildren: () => import('./features/infinite-list/feature-infinite-list.module').then(m => m.FeatureInfiniteListModule) },
    { path: 'movies', loadChildren: () => import('./features/movies/feature-movies.module').then(m => m.FeatureMoviesModule) },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
