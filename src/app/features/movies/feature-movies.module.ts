import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FeatureMoviesRoutingModule } from './feature-movies-routing.module';

import { CoreMaterialModule } from 'src/app/core/material/material.module';

import { MoviesComponent } from './movies/movies.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule, ReactiveFormsModule,
		CoreMaterialModule,

		FeatureMoviesRoutingModule,
	],
	declarations: [
		MoviesComponent,
    ],
	providers: [],
})
export class FeatureMoviesModule { }
