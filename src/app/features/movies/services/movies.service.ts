import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MoviesService {

	uri: string = environment.serverMoviesUrl

	constructor(
		private http: HttpClient
	) { }
	
	getMovies(): Observable<any> {
		return this.http.get<any>(this.uri)
			.pipe(
                map((res: HttpResponse<any>) => res, catchError((error: HttpErrorResponse) => throwError(error)))
            )
		}
	;

}
