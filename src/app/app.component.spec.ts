import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppComponent } from './app.component';
import { DialogComponent } from './core/components/dialog/dialog.component';

import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


describe('AppComponent', () => {
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                BrowserDynamicTestingModule,
                BrowserAnimationsModule,
                MatSnackBarModule,
                MatDialogModule,

            ],
            declarations: [
                AppComponent
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: []},
            ]
        })
        .overrideModule(BrowserDynamicTestingModule, {
            set: {
              entryComponents: [DialogComponent]
            }
        })
        .compileComponents();
    });

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
    });

    // -- // --
    
    it('should call openDialog', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.componentInstance;
        const expected_header = "Dialog Header";

        app.openDialog('primary');

        fixture.detectChanges();
        const dialogHeader = document.getElementsByTagName('h2')[0] as HTMLHeadElement;
        expect(dialogHeader.innerText).toEqual(expected_header);

        // ... and now close it:
        //
        // ??????
    })
});

