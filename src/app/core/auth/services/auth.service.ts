import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { environment } from 'src/environments/environment';
import { User } from '../auth.interfaces';

@Injectable({
    providedIn: 'root',
})
export class AuthService {

	uri: string = environment.serverUrl
	
	constructor(private http: HttpClient) { }
	
	login(email: string, password: string): Observable<User | any> {
		return this.http.post<User | any>(this.uri + 'authenticate', {email: email, password: password})
			.pipe(
                map((res: HttpResponse<User>) => res, catchError((error: HttpErrorResponse) => throwError(error)))
            )
		}
	;

	logout() {
		localStorage.removeItem('authData');
	}

	public get isLoggedIn(): boolean {
		return(localStorage.getItem('authData') !== null);
	}

}
