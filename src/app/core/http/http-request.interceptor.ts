import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, Subscription, throwError } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';

import { LoaderService } from '../components/services/loader.service';
import { NotificationService } from '../../services/notification.service';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

    loaderSubscription: Subscription = this.loaderService.spinner$.subscribe();

    constructor(
        public loaderService: LoaderService,
		private notification: NotificationService,
        private router: Router
    ) { }

    public getAuthorizationToken() {
        return this.getAccessToken() !== '' ? 'Basic ' + this.getAccessToken() : '????'

    }

    public getAccessToken() {
        // const authData: string | null = localStorage.getItem('authData');
	    // return authData !== null ? JSON.parse(authData).auth_token : '';
        return '61e24e848bdc3e4e93f527dbc6f85cb8:690a235bec49ca0bf53cd3f83dc70d9e'
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // We replace 'req' by 'authRequest':
        const authRequest = req.clone({
            setHeaders: {
                Authorization: this.getAuthorizationToken(),
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,OPTIONS',
                'Access-Control-Allow-Headers': 'Access-Control-Allow-Origin,*',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Allow-Origin': '*',
            }
        });
        // 2) I.e., set basic headers:
        //      if (!request.headers.has('Content-Type')) {
        //          request = request.clone({ headers: request.headers.set('Content-Type', 'application/json')});
        //      }
        // 3) Caching behavior - for example, If-Modified-Since...
        // 4) XSRF protection
        // etc, etc.
        // console.warn('request interception:', this.getAuthorizationToken());


        const time = performance.now();

        return next.handle(authRequest)
            .pipe(
                map( (event: HttpEvent<any>) => {

                    if (event instanceof HttpResponse) {
                        console.warn(event);
                        this.loaderSubscription.unsubscribe();

                        this.logRequestTime(time, req.url, req.method);
                        
                        if(event.url === 'http://localhost:5000/api/authenticate') {
                            this.notification.success('Nice! You\'are now loged in!');
                        }
                    }

                    return event
                }),
                catchError((error: HttpErrorResponse): Observable<HttpEvent<any>> => this.errorHandler(error))
            );
    }

    private logRequestTime(initTimming: number, url: string, method: string) {
        const requestDuration: number = performance.now() - initTimming;
        console.warn(`HTTP ${method} ${url} - ${requestDuration} ms`);
    }

    /**
     * Customize the default error handler here if needed
     * @param response 
     * @returns handler 
     */
     private errorHandler(response: HttpErrorResponse): Observable<HttpEvent<any>> {
        this.loaderSubscription.unsubscribe();

        const errormessage = `URL: ${response.url}\nStatus: ${response.status} StatusText: ${response.statusText}`;
        // this.notification.default(errormessage);
        // this.notification.info(errormessage);
        // this.notification.success(errormessage);
        // this.notification.warn(errormessage);
        this.notification.error(errormessage);

        if (response.status === 401 && (window.location.href.match(/\?/g) || []).length < 2) {
            console.warn('Session expired/unauthorized => redirecting to "/login".');
            this.router.navigate(['/login']);
        }
        if (response.status === 403 && (window.location.href.match(/\?/g) || []).length < 2) {
            console.warn('Incorrect credentials => please re-authenticate...');
            this.notification.warn('Incorrect credentials -> please re-authenticate...');
        }

        return throwError(response.error);
    }

}