import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { environment } from 'src/environments/environment';

import { DialogComponent } from './core/components/dialog/dialog.component';
import { NotificationService } from './services/notification.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	title = 'ng11-enterprise';

	constructor(
		private dialog: MatDialog,
		private notification: NotificationService
	) { }

	openDialog(themeColor: 'primary' | 'accent' | 'warn'): void {

		// const dialogConfig = new MatDialogConfig();
		// dialogConfig.data = themeColor;
		// dialogConfig.panelClass = 'custom-dialog';
		// dialogConfig.backdropClass = 'backdropBackground'
		// setTimeout(() => {
		// 	const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
		// }, 500);

		const dialogRef = this.dialog.open(DialogComponent, {
			panelClass: 'custom-dialog',
			data: {	themeColor },
			// backdropClass: 'backdropBackground'
		});
	}
}
