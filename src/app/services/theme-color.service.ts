import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ThemeColorService {

    public themeColor$: Observable<any>;
    private colorSubject = new BehaviorSubject('primary');

    constructor() {
        this.themeColor$ = this.colorSubject.asObservable();
    }

    passedVar(color: 'primary' | 'accent' | 'warn'): void {
        this.colorSubject.next(color);
	}

}
